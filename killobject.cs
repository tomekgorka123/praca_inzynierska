﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class killobject : MonoBehaviour
{

    public float lifeTime = 0f;
    public float changeTime = 0f;
    System.Random rnd;
    public bool destroyed = false;
  
    void Update()
    {

		if(changeTime < 1){
			changeTime += Time.deltaTime;
			if(changeTime >= 1){
				rnd = new System.Random(this.GetInstanceID()+(int)lifeTime);
		    		int chance = rnd.Next(0, 100);
	
		   		if(chance>=80 ){
					Debug.Log(this.GetInstanceID() + " chance death");
		    			Destruction();	
					destroyed = true;

		    		}
	
		    		if(chance<20){
					Debug.Log(this.GetInstanceID() + " chance born");
					spawnobject.quantity++;
					Object pre = Resources.Load("Organism", typeof(GameObject));
		 			var pos = new Vector3(Random.Range(-15,15), 0, Random.Range(-15,15));
					var obj = GameObject.Instantiate(pre, pos, Quaternion.identity);
	
		    		}
		    
				changeTime = changeTime % 1f;
		   		
			}
		}

	        if((destroyed == false) && (lifeTime < 5.0f)) {

			lifeTime += Time.deltaTime;
			if(lifeTime >= 5.0f) {
				Debug.Log(this.GetInstanceID() + " time death");			
				Destruction();
				
			}
		}
    }
    void Destruction() {
	spawnobject.quantity--;
        Destroy(this.gameObject);
    }

}
