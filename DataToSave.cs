﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DataToSave {

	List<int> data;

	public DataToSave (List<int> list){
		data = list;
	}
}