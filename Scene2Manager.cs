﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scene2Manager : MonoBehaviour
{


    void Start()
    {

        Object preOrg = Resources.Load("Organism2", typeof(GameObject));
	Object preMeal = Resources.Load("Meal", typeof(GameObject));
	for(int i=0; i<20; i++) {

		var pos = new Vector3(0,0,0);


		int verticalOrHorizontal = Random.Range(0,2);

		if(verticalOrHorizontal == 0){
			int lorp = Random.Range(0,2);
			if(lorp == 0){
				pos = new Vector3(Random.Range(-19.5f, 19.5f), 1, 19.5f);
			}
			if(lorp == 1){
				pos = new Vector3(Random.Range(-19.5f, 19.5f), 1, -19.5f);
			}
		}

		if(verticalOrHorizontal == 1){
			int uorp = Random.Range(0,2);
			if(uorp == 0){
				pos = new Vector3(-19, 1, Random.Range(-19.5f, 19.5f));
			}
			if(uorp == 1){
				pos = new Vector3(19, 1, Random.Range(-19.5f, 19.5f));
			}
		}

		var obj = GameObject.Instantiate(preOrg, pos, Quaternion.identity);

	}

	for(int i=0; i<100; i++) {
		var pos = new Vector3(Random.Range(-17.0f,17.0f), 0.25f, Random.Range(-17.0f,17.0f));
		var obj2 = GameObject.Instantiate(preMeal, pos, Quaternion.identity);
	}
	
    }

    void Update()
    {
	


    }
    void OnApplicationQuit()
    {
        
    }
}
