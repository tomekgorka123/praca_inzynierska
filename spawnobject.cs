﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnobject: MonoBehaviour
{
    public List<int> dane = new List<int>();
    public float saveTime = 0f;
    public static int quantity = 0;        
    void Start()
    {
        Object pre = Resources.Load("Organism", typeof(GameObject));
	for(int i=0; i<5; i++) {

		var pos = new Vector3(Random.Range(-15,15), 0.1f, Random.Range(-15,15));

		var obj = GameObject.Instantiate(pre, pos, Quaternion.identity);
		quantity++;
	}
    }

    void Update()
    {
	
	if(saveTime < 1){
		saveTime += Time.deltaTime;
		if(saveTime >= 1){
			Debug.Log(quantity);	
			dane.Add(quantity);
			saveTime = saveTime%1.0f;

		}
	}
 	

	if(quantity<=0){
		Application.Quit();
	}

    }
    void OnApplicationQuit()
    {
        SaveSystem.saveData(dane);
    }
}
